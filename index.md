---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
<div id="welcome" class="uk-section section-no-color animated fadeIn delay-4s fast">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Título</h2>
        <p class="font-lead">Bajada</p>
        <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipisicing elit</a>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ç. </p>
        <button class="uk-button button-color-1st">Botón</button>
        <button class="uk-button button-color-2nd">Botón</button>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-color-1st animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Título</h2>
        <p class="font-lead">Bajada</p>
        <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipisicing elit</a>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ç. </p>
        <button class="uk-button button-color-1st">Botón</button>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-color-2nd animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Título</h2>
        <p class="font-lead">Bajada</p>
        <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipisicing elit</a>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ç. </p>
        <button class="uk-button button-color-2nd">Botón</button>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-gradient animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Título</h2>
        <p class="font-lead">Bajada</p>
        <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipisicing elit</a>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ç. </p>
        <button class="uk-button button-white-color-2nd">Botón</button>
        <button class="uk-button button-white-color-1st">Botón</button>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-black animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Título</h2>
        <p class="font-lead">Bajada</p>
        <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipisicing elit</a>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ç. </p>
        <button class="uk-button button-white-black">Botón</button>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-black-soft animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Título</h2>
        <p class="font-lead">Bajada</p>
        <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipisicing elit</a>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ç. </p>
        <button class="uk-button button-white-black-soft">Botón</button>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-grey animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Título</h2>
        <p class="font-lead">Bajada</p>
        <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipisicing elit</a>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ç. </p>
        <button class="uk-button button-white-grey">Botón</button>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-grey-soft animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Título</h2>
        <p class="font-lead">Bajada</p>
        <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipisicing elit</a>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ç. </p>
        <button class="uk-button button-black-grey-soft">Botón</button>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-grey-light animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Título</h2>
        <p class="font-lead">Bajada</p>
        <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipisicing elit</a>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ç. </p>
        <button class="uk-button button-black-grey-light">Botón</button>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-grey-light-soft animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center " uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Título</h2>
        <p class="font-lead">Bajada</p>
        <p>Lorem ipsum dolor sit amet, <a href="#">consectetur adipisicing elit</a>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ç. </p>
        <button class="uk-button button-black-grey-light-soft">Botón</button>
      </div>
    </div>
  </div>
</div>
