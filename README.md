# Template de páginas en Jekyll para Ciudadanía Inteligente

Este template puede ser visto en https://ciudadaniai.gitlab.io/template-jekyll/


## 1. Comenzar un repositorio a partir de este template

- Descargar este repositorio: para ello en la página principal seleccionar Download > Download source code.
- Descomprimir los archivos y darle el nombre a la carpeta del proyecto.
- Crear un nuevo repositorio en el gitlab de la organización [https://gitlab.com/ciudadaniai] para subir estos archivos.
- Conectar la carpeta local del proyecto con el repositorio git. Para ello *pushear* la carpeta existente:

```
cd <nombre-carpeta> //Entrar, utilizando la terminal, en el carpeta donde se encuentra el proyecto localmente
git init
git remote add origin git@gitlab.com:ciudadaniai/<nombre-repositorio>.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

## 2. Configurar el nuevo repositorio

- Modificar el archivo de configuración: Percatarse de actualizar en el archivo `_config.yml` los diferentes atributos. Indicar en **url** cuál sera el dominio que se utilizará, y dejar en blanco **baseurl** (en la mayoría de los casos será así, es decir una web será ejemplo.org, y no ejemplo.org/web). Un ejemplo:
```
baseurl: ""
url: "https://ejemplodominio.org"
```
- Crear gitignore: Crear un archivo llamado `.gitignore` en la carpeta principal, el que evitará que se suban archivos innecesarios cada vez que hacemos un commit, con lo siguiente:
```
_site
.sass-cache
.jekyll-metadata
```
- Crear el archivo de deploy: Crear un archivo llamado `.gitlab-ci.yml` en la carpeta principal que hará deploy de la web cada vez que haya un cambio en master. El archivo debe contener lo siguiente:
```
image: ruby:latest

variables:
  JEKYLL_ENV: production
  LC_ALL: C.UTF-8

before_script:
  - gem install bundler
  - bundle install

test:
  stage: test
  script:
  - bundle exec jekyll build -d test
  artifacts:
    paths:
    - test
  except:
  - master

pages:
  stage: deploy
  script:
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
```

## 3. Agregar Google Analytics

En proceso

## 4. Configurar el dominio

En proceso


## Instalación

- Install jekyll
```
gem install bundler jekyll
```
- Clone repository
```
git@gitlab.com:ciudadaniai/template-jekyll.git
```
- ` cd template-jekyll` to enter the site
- ` bundle install` to install the gems
- Ready!
```
bundle exec jekyll serve --watch --baseurl=    
```
